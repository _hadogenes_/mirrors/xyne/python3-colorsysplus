#!/usr/bin/env python3

# Copyright (C) 2012-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'''
Support for colors in format, percent-style and template strings.
'''

import re
import string

from colorsysplus import hex_to_rgb
from colorsysplus.text import ColorizedText


# ------------------------------ ColorFormatter ------------------------------ #

# https://docs.python.org/3/library/string.html#string.Formatter
class ColorFormatter(string.Formatter):
    '''
    Subclass of string.Formatter that supports color codes in format
    specification strings.
    '''
    def __init__(self, *args, color_char='@', output_type='ansi', **kwargs):
        '''
        Args:
            *args, **kwargs:
                Positional and keyword arguments passed through to
                string.Formatter.

            color_char:
                The character used to indicate a color after the type in the
                format specification. With the default value "@", the following
                will insert the value of field "red" and color it red:
                "{red:s@f00}".  This should be set to a value that will not
                occur elsewhere in the format specification.

            output_type:
                The  color output type.
        '''
        super().__init__(*args, **kwargs)
        self.color_char = color_char
        self.output_type = output_type

    def _parse_color(self, format_spec):
        try:
            format_spec, hexcolor = format_spec.rsplit(self.color_char, 1)
            rgb = hex_to_rgb(hexcolor)
        except ValueError:
            return format_spec, None
        else:
            return format_spec, rgb

    def format_field(self, value, format_spec):
        format_spec, rgb = self._parse_color(format_spec)
        if rgb:
            value = super().format_field(value, format_spec)
            return ColorizedText(chunks=((rgb, value),)).to_x(self.output_type)
        return super().format_field(value, format_spec)

    def colorize(self, fmt, output_type=None):
        '''
        Add colors to the format.
        '''
        if output_type is None:
            output_type = self.output_type
        cfmt = ''
        for text, name, spec, conv in self.parse(fmt):
            cfmt += text
            # Name is None if there is no field.
            if name is not None:
                rgb = None
                field = name if name else ''
                if conv:
                    field += f'!{conv}'
                if spec:
                    spec, rgb = self._parse_color(spec)
                    field += f':{spec}'
                field = f'{{{field}}}'
                if rgb:
                    field = ColorizedText(chunks=((rgb, field),)).to_x(output_type)
                cfmt += field
        return cfmt


# ------------------------------ ColorTemplate ------------------------------- #

class ColorTemplate(string.Template):
    '''
    Subclass of string.Template that supports color codes affixed to
    identifiers.
    '''

    def __init__(self, template, color_char='@', output_type='ansi'):
        '''
        Args:
            template:
                The template string, with optional color codes after the
                identifiers.

            color_char:
                The character used to indicate a color after the identifier.
                With the default value "@", the string "${var@00f}" would be
                replaced by the value of "var" and colored blue after a call to
                substitute. The character must not be a valid identifier
                character.

            output_type:
                The  color output type.
        '''
        # Modified from string.Template
        delim = re.escape(self.delimiter)
        idpattern = self.idpattern
        bidpattern = self.braceidpattern if self.braceidpattern else idpattern
        color_char = re.escape(color_char)
        colorpattern = r'[0-9A-Fa-f]+'

        pattern = fr"""
        {delim}(?:
            (?P<escaped>{delim})                                                 |
            (?P<named>{idpattern}){color_char}(?P<ncolor>({colorpattern}))       |
            {{(?P<braced>{bidpattern}){color_char}(?P<bcolor>({colorpattern}))}} |
            (?P<invalid>)
        )
        """
        self.color_char = color_char
        self.output_type = output_type
        regex = re.compile(pattern, self.flags | re.VERBOSE)
        self.colored_template = regex.sub(self._add_colors, template)
        super().__init__(self.colored_template)

    def _colorize(self, name, color, fmt):
        delim = self.delimiter
        placeholder = '*' * 20
        rgb = hex_to_rgb(color)
        text = ColorizedText(chunks=((rgb, placeholder),)).to_x(self.output_type)
        return text.replace(delim, delim * 2).replace(placeholder, fmt.format(name))

    def _add_colors(self, match):
        color = match['ncolor']
        if color:
            return self._colorize(match['named'], color, '${}')

        color = match['bcolor']
        if color:
            return self._colorize(match['braced'], color, '${}')

        return match.group(0)


# ---------------------------- ColorPercentStyle ----------------------------- #

# See https://docs.python.org/3/library/stdtypes.html#old-string-formatting.
class ColorPercentStyle():  # pylint: disable=too-few-public-methods
    '''
    Colorize percent-style format strings with custom color codes.
    '''

    def __init__(self, fmt, color_char='@', output_type='ansi'):
        self.fmt = fmt
        self.color_char = color_char
        self.output_type = output_type

        delim = re.escape('%')
        color_char = re.escape(color_char)
        colorpattern = r'[0-9A-Fa-f]+'
        # From logging.PercentStyle.validation_pattern.pattern
        # re.compile(r'%\(\w+\)[#0+ -]*(\*|\d+)?(\.(\*|\d+))?[diouxefgcrsa%]', re.I)
        percent_pattern = r'(?:\(\w+\))*[#0+ -]*(?:\*|\d+)?(?:\.(\*|\d+))?[diouxefgcrsa%]'
        pattern = fr"""
        {delim}(?:
            (?P<escaped>{delim})                                                 |
            (?P<substitution>{percent_pattern}){color_char}(?P<color>({colorpattern}))
        )
        """
        self.regex = re.compile(pattern, re.I | re.VERBOSE)
        self.colored_format = self.regex.sub(self._add_colors, self.fmt)

    def _add_colors(self, match):
        color = match['color']
        if color:
            placeholder = '*' * 20
            rgb = hex_to_rgb(color)
            text = ColorizedText(chunks=((rgb, placeholder),)).to_x(self.output_type)
            return text.replace('%', '%%').replace(placeholder, f'''%{match['substitution']}''')

        return match.group(0)

    def __mod__(self, arg):
        '''
        Colorize a percent-style format string.
        '''
        return self.colored_format % arg


# ------------------------------- colorize_fmt ------------------------------- #

def colorize_fmt(fmt, style='{', output_type='ansi', color_char='@'):
    '''
    Colorize a format or template string.

    Args:
        fmt:
            A format or template string in one of the supported styles.

        style:
            One of "{", "%" or "$", which correspond to Python format strings,
            percent-style strings or templates, respectively.

        output_type:
            The output type.

    Returns:
        A format string with color codes.
    '''
    if style == '%':
        return ColorPercentStyle(
            fmt,
            color_char=color_char,
            output_type=output_type
        ).colored_format

    if style == '{':
        return ColorFormatter(
            color_char=color_char,
            output_type=output_type
        ).colorize(fmt)

    if style == '$':
        return ColorTemplate(
            fmt,
            color_char=color_char,
            output_type=output_type
        ).colored_template

    raise ValueError(f'unsupported style: "{style}"')


# ------------------------------- color_format ------------------------------- #

def format_with_color(fmt, fields, style='{', output_type='ansi', color_char='@'):
    '''
    Format a string.

    Args:
        fmt:
            A format or template string in one of the supported styles.

        fields:
            A dictionary mapping field names to values.

        style:
            One of "{", "%" or "$", which correspond to Python format strings,
            percent-style strings or templates, respectively.

        output_type:
            The output type.

    Returns:
        The formatted string.
    '''
    if style == '{':
        fmter = ColorFormatter(color_char=color_char, output_type=output_type)
        return fmter.format(fmt, **fields)

    if style == '$':
        fmter = ColorTemplate(fmt, color_char=color_char, output_type=output_type)
        return fmter.substitute(fields)

    if style == '%':
        fmter = ColorPercentStyle(fmt, color_char=color_char, output_type=output_type)
        return fmter % fields

    raise ValueError(f'unrecognized color style in record: "{style}"')


# --------------------------------- Testing ---------------------------------- #

def run_example():  # pylint: disable=too-many-locals
    '''
    Print different output formats for each format style.
    '''

    examples = (
        ('f00', 'red'),
        ('f80', 'orange'),
        ('ff0', 'yellow'),
        ('0f0', 'green'),
        ('0ff', 'cyan'),
        ('00f', 'blue'),
        ('80f', 'violet')
    )

    def get_fmt_and_values(fmt):
        words = list()
        values = dict()
        for i, (color, name) in enumerate(examples):
            field = f'arg{i}'
            words.append(fmt.format(field=field, color=color))
            values[field] = name
        return ' '.join(words), values

    print('Examples Of Colorized Outputs:')

    # Format strings.
    fmt, values = get_fmt_and_values('{{{field}:@{color}}}')
    print(
        '\n* Format Strings\n'
        f'   input:            {fmt}\n'
        f'   plain output:     {ColorFormatter(output_type="plain").format(fmt, **values)}\n'
        f'   ansi output:      {ColorFormatter(output_type="ansi").format(fmt, **values)}\n'
        f'   colorized format: {ColorFormatter(output_type="ansi").colorize(fmt)}\n'
    )

    # Templates.
    fmt, values = get_fmt_and_values('${{{field}@{color}}}')
    print(
        '\n* Templates\n'
        f'   input:            {fmt}\n'
        f'   plain output:     {ColorTemplate(fmt, output_type="plain").substitute(values)}\n'
        f'   plain output:     {ColorTemplate(fmt, output_type="ansi").substitute(values)}\n'
        f'   colorized format: {ColorTemplate(fmt, output_type="ansi").colored_template}\n'
    )

    # Percent Style.
    fmt, values = get_fmt_and_values('%({field})s@{color}')
    print(
        '\n* Percent-Style\n'
        f'   input:            {fmt}\n'
        f'   plain output:     {ColorPercentStyle(fmt, output_type="plain") % values}\n'
        f'   plain output:     {ColorPercentStyle(fmt, output_type="ansi") % values}\n'
        f'   colorized format: {ColorPercentStyle(fmt, output_type="ansi").colored_format}\n'
    )


if __name__ == '__main__':
    try:
        run_example()
    except (KeyboardInterrupt, BrokenPipeError):
        pass
