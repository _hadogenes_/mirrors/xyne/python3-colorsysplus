#!/usr/bin/env python3

# Copyright (C) 2012-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# pylint: disable=invalid-name

'''
This module supplements the standard colorsys module with functions for
converting RGB to and from the following:

  * CMYK
  * hexadecimal color strings (e.g. HTML colors)
  * terminal color indices & ANSI escape codes
  * Xonotic color codes

Some functionality may be incomplete. See the code and documention for details.
'''

import cmath
import re
import select
import sys
import termios
from colorsys import (
    hls_to_rgb,
    hsv_to_rgb,
    rgb_to_hls,
)


# --------------------------------- Globals ---------------------------------- #

# Discrete color and greyscale steps of the terminal colors determined by direct
# measurement. These should be valid for default colors in 256-color terminals.
# This is further confirmed here:
# http://stackoverflow.com/questions/27159322/rgb-values-of-the-colors-in-the-ansi-extended-colors-index-17-255
TERM_256_COLOR_STEPS = tuple(x / 255 for x in (0, 95, 135, 175, 215, 255))
TERM_256_GREYSCALE_STEPS = tuple(x / 255 for x in range(8, 240, 10))

TERM_BASIC_COLORS = (
    (0.0, 0.0, 0.0),
    (1.0, 0.0, 0.0),
    (0.0, 1.0, 0.0),
    (1.0, 1.0, 0.0),
    (0.0, 0.0, 1.0),
    (1.0, 0.0, 1.0),
    (0.0, 1.0, 1.0),
    (1.0, 1.0, 1.0)
)
TERM_BASIC_COLOR_NAMES = (
    'black',
    'red',
    'green',
    'yellow',
    'blue',
    'magenta',
    'cyan',
    'white'
)


# https://en.wikipedia.org/wiki/ANSI_escape_code
# Control Sequence Introducer/Initiator
ANSI_ESC = chr(0x1b)  # \033
ANSI_SGR_REGEX = re.compile('({}\\[.+?m)'.format(ANSI_ESC))

ANSI_SGR_KEYWORDS_TO_PARAMS = {
    'reset': '0',
    'bold': '1',
    'blink': '5',
    'negative': '7',
}
ANSI_SGR_PARAMS_TO_KEYWORDS = {v: k for k, v in ANSI_SGR_KEYWORDS_TO_PARAMS.items()}

# Abbreviated color escapes and their matching hexadecimal escapes.
XONOTIC_SHORT_TO_LONG = {
    r'0': r'x000',
    r'1': r'xf00',
    r'2': r'x0f0',
    r'3': r'xff0',
    r'4': r'x00f',
    r'5': r'x0ff',
    r'6': r'xf0f',
    r'7': r'xfff',
    r'8': r'x888',
    r'9': r'x888',
}
XONOTIC_LONG_TO_SHORT = dict((v, k) for k, v in XONOTIC_SHORT_TO_LONG.items())


# ------------------------------- CMYK Colors -------------------------------- #

def rgb_to_cmyk(r, g, b):
    '''
    Convert RGB to CMYK.

    This seems to be one of the most commonly used algorithms but CMYK values
    vary between printers and there is no canonical formula. Do not rely on these
    values for anything more than qualitative approximations.
    '''
    k = min(1.0 - r, 1.0 - g, 1.0 - b)
    if k < 1.0:
        d = 1.0 - k
        c = (d - r) / d
        m = (d - g) / d
        y = (d - b) / d
    else:
        c = 0.0
        m = 0.0
        y = 0.0
    return c, m, y, k


def cmyk_to_rgb(c, m, y, k):
    '''
    Convert CMYK to RGB.

    This seems to be one of the most commonly used algorithms but CMYK values
    vary between printers and there is no canonical formula. Do not rely on these
    values for anything more than qualitative approximations.
    '''
    d = k
    f = 1.0
    return tuple((1.0 - x * f - d) for x in (c, m, y))


# ---------------------------- Hexadecimal Colors ---------------------------- #

def hex_to_rgb(hexstr, alpha=False):
    '''
    Convert a hexadecimal color string to RGB(A).

    Variable widths are recognized (e.g. #40f, #4400ff, #444000fff). The hash
    is optional. If the string length is a multiple of 3 then it is
    interpreted as RGB unless "alpha" is True. If it is a multiple of 4 then it
    is interpreted as RGBA. Any other multiple raises a ValueError.

    Args:
        hexstr:
            An RGB(A) color in hexadecimal format, with option leading "#".

        alpha:
            If True, the string is interpretted as an RGBA color even if the
            length is a multiple of 3.

    Returns:
        A 3-tuple or RGB values in the range [0-1.0] or a 4-tuple of RGBA values
        in the same range.

    Raises:
        ValueError if hexstr is not a hexadecimal string with a length that is a
        multiple of 3 or 4.
    '''
    hexstr = hexstr.lstrip('#')
    hexstr_len = len(hexstr)
    if hexstr_len % 3 == 0 and not alpha:
        n_channels = 3
    elif hexstr_len % 4 == 0:
        n_channels = 4
    else:
        raise ValueError('"{}" is not a valid RGB or RGBA hexademical color'.format(hexstr))
    width = hexstr_len // n_channels
    base = 0x10**width - 1
    return tuple(int(hexstr[i * width:(i + 1) * width], 16) / base for i in range(n_channels))


def rgb_to_hex(r, g, b, alpha=None, width=2, include_hash=True):  # pylint: disable=too-many-arguments
    '''
      Convert RGB to a hexadecimal color string.

      alpha: Optional alpha channel value, in the range [0.0, 1.0].

      width: The number of characters to use for each field.

      include_hash: Prefix the string with a hash.
    '''
    if alpha is not None:
        xs = (r, g, b, alpha)
    else:
        xs = (r, g, b)
    base = 0x10**width - 1
    xs = tuple(round(x * base) for x in xs)
    n = len(xs)
    fmt = '{{:0{:d}x}}'.format(width) * n
    if include_hash:
        fmt = '#' + fmt
    return fmt.format(*xs)


# ----------------------------- Terminal Colors ------------------------------ #

def term_get_closest(arg, values):
    '''
    Return the value in values that is closest to arg in absolute distance.

    arg and values must lie in the range [0.0, 1.0] and values must be sorted in
    increasing order.
    '''
    closest = None
    if arg < 0.0:
        arg = 0.0
    elif arg > 1.0:
        arg = 1.0
    # The values lie in the range [0.0, 1.0] and so the difference will never
    # exceed 1. Use 1.1 to ensure that the first value satisfies the condition.
    # The values are given in increasing order and so the difference should
    # decrease until the closes match is found.
    shortest = 1.1
    for i, val in enumerate(values):
        d = abs(arg - val)
        if d < shortest:
            closest = (i, val)
            shortest = d
        else:
            break
    return closest


def term_get_closest_256_color(arg):
    '''
    Get the closest matching terminal color for the given argument.
    '''
    return term_get_closest(arg, TERM_256_COLOR_STEPS)


def term_get_closest_256_greyscale(arg):
    '''
    Get the closest matching terminal greyscale value for the given argument.
    '''
    return term_get_closest(arg, TERM_256_GREYSCALE_STEPS)


def term_to_rgb(index, n=256, colors=None):
    '''
    Convert a terminal color index to RGB.

    n: The number of colors to use. Currently only 256 is supported, but see
    "colors" below.

    colors: A list of RGB values corresponding to terminal colors. The index of
    the list is the terminal color index. Returns None on IndexError.

    If n is not 256 and no colors are provided, returns None.
    '''
    if colors:
        try:
            return colors[index]
        except IndexError:
            return None
    if n == 256:
        if 16 <= index < 232:
            index -= 16
            index, b = divmod(index, 6)
            r, g = divmod(index, 6)
            return tuple(TERM_256_COLOR_STEPS[i] for i in (r, g, b))
        if 232 <= index < 256:
            index -= 232
            r = TERM_256_GREYSCALE_STEPS[index]
            return r, r, r
        index %= 8
        return TERM_BASIC_COLORS[index]
    return None


def rgb_to_term(r, g, b, n=256, colors=None):
    '''
    Convert RGB to a terminal color index.

    n:
      The number of colors to use. Currently only 256 is supported, but see
      "colors" below.

    colors:
      A list of RGB values corresponding to terminal colors. The index of the list
      is the terminal color. Returns the best approximation using a least squares
      method. The values should be in the range of 0.0 to 1.0.

    If n is not 256 and no colors are provided, returns None.
    '''
    if colors:
        closest = 0
        shortest = 4
        for i, color in enumerate(colors):
            x, y, z = color
            d = (r - x)**2 + (g - y)**2 + (b - z)**2
            if d < shortest:
                closest = i
                shortest = d
        return closest

    # Blindly determine color codes by assuming standard values for the color
    # cube and greyscale.
    if n == 256:
        if r == g == b:
            return term_get_closest_256_greyscale(r)[0] + 232
        r, g, b = (term_get_closest_256_color(x)[0] for x in (r, g, b))
        return 16 + b + (g * 6) + (r * 36)

    return None


# --------------- Terminal Colors Through Terminal Interaction --------------- #

class QuietUnbufferedUnblockedTerminal():
    '''
    A wrapper object to configure the terminal for determining current RGB color
    values.
    '''

    def __init__(self):
        self.tc_save = None

    def __enter__(self):
        return self.term_init()

    def __exit__(self, typ, val, traceback):
        self.term_reset()

    def term_init(self):
        '''
        Initialize the terminal.

        This does the following to facilitate terminal interaction for determining
        color values:

          * disables echo
          * disables buffering
          * disables blocking

        This should be called before invoking term_to_queried_rgb().

        Returns a terminal save state to pass to term_reset().
        '''
        self.tc_save = None
        poll = select.poll()
        poll.register(sys.stdin.fileno(), select.POLLIN)
        try:
            self.tc_save = termios.tcgetattr(sys.stdout.fileno())
            tc_attr = termios.tcgetattr(sys.stdout.fileno())

            # Disable echo
            tc_attr[3] &= ~termios.ECHO

            # Disable buffering
            tc_attr[3] &= ~termios.ICANON

            # Disable blocking
            tc_attr[6][termios.VMIN] = 0
            tc_attr[6][termios.VTIME] = 0

            termios.tcsetattr(sys.stdout.fileno(), termios.TCSANOW, tc_attr)

        except Exception as exc:
            self.term_reset()
            raise exc

        return poll

    def term_reset(self):
        '''
        Reset the terminal.

        See term_init().
        '''
        if self.tc_save:
            termios.tcsetattr(
                sys.stdout.fileno(),
                termios.TCSANOW,
                self.tc_save
            )
            self.tc_save = None


# The following is adapted from code posted by Lux Perpetua on the Arch Linux
# forums:
#
#   https://bbs.archlinux.org/viewtopic.php?pid=1179708#p1179708

# echo -en "\033]4;132;?\007"

def term_to_queried_rgb(index, poll, timeout=-1, retries=5):
    '''
    Convert a terminal color index to RGB values.

    This should be called within a `with` block using
    QuietUnbufferedUnblockedTerminal.

    poll: poll object returned by QuietUnbufferedUnblockedTerminal's enter method.

    timeout: The timeout when polling.

    retries: The maximum number of retries when reading from stdout.
    '''
    query = '\033]4;{:d};?\007'.format(index)

    # Discard just in case.
    while poll.poll(0):
        sys.stdin.read()
    sys.stdout.write(query)
    sys.stdout.flush()

    regex = re.compile(
        "\033\\]([0-9]+;)+rgba?:([0-9a-fA-F]+)/([0-9a-fA-F]+)/([0-9a-fA-F]+)(/([0-9a-fA-F]+))?"
    )
    match = None
    output = ''

    # Attempt to read expected output from stdin.
    # Give up after a timeout or a limited number of retries
    while not match:
        if retries < 1 or not poll.poll(timeout):
            return None
        retries -= 1
        output += sys.stdin.read()
        match = regex.search(output)

    width = len(match.group(2))
    base = 0x10**width - 1

    # Group 5 is only present if there is an alpha channel. The value apparently
    # precedes the RGB values.
    if match.group(5):
        rgb_indices = [3, 4, 6]
    else:
        rgb_indices = [2, 3, 4]

    return tuple(int(match.group(i), 16) / base for i in rgb_indices)


def get_term_colors(timeout=1000):
    '''
    Iterate over the real RGB values for the current terminal.
    '''
    with QuietUnbufferedUnblockedTerminal() as poll:
        try:
            i = 0
            while True:
                rgb = term_to_queried_rgb(i, poll, timeout=timeout)
                if rgb:
                    yield rgb
                    i += 1
                else:
                    break
        except KeyboardInterrupt:
            pass


# -------------------------- ANSI Escape Sequences --------------------------- #

def remove_ansi(text):
    '''
    Remove ANSI escape codes from a string.
    '''
    return ANSI_SGR_REGEX.sub('', text)


def ansi_sgr_color(color, fg=True, truecolor=False, term_colors=None):
    '''
    Get the appropriate sequence for the given color. If truecolor is false,
    colors will be cast to nearest color index.
    '''
    if fg:
        base = 30
    else:
        base = 40

    if isinstance(color, int):
        color = color % 0x100
        if color > 7:
            return '{:d};5;{:d}'.format(base + 8, color)
        return '{:d}'.format(base + color)

    if isinstance(color, str):
        try:
            return '{:d}'.format(base + TERM_BASIC_COLOR_NAMES.index(color))
        except ValueError:
            pass
        rgb = hex_to_rgb(color)
    else:
        rgb = color

    if truecolor:
        rgb = tuple(round(x * 0xff) for x in rgb)
        return '{:d};2;{:d};{:d};{:d}'.format(base + 8, *rgb)

    i = rgb_to_term(*rgb, colors=term_colors)
    return '{:d};5;{:d}'.format(base + 8, i)


# sgr: select graphic rendition
def ansi_sgr(
    fg=None,
    bg=None,
    term_colors=None,
    **kwargs
):
    '''
    Get the ANSI escape sequence for the selected graphic rendition. This function
    accepts all keywords in ANSI_SGR_KEYWORDS_TO_PARAMS, the values of which
    should be boolean and indicate if that parameter should be set.
    '''
    fields = list()
    for key, value in kwargs.items():
        try:
            param = ANSI_SGR_KEYWORDS_TO_PARAMS[key]
        except KeyError:
            pass
        else:
            if value:
                fields.append(param)
    if fg is not None:
        fields.append(ansi_sgr_color(fg, fg=True, term_colors=term_colors))
    if bg is not None:
        fields.append(ansi_sgr_color(fg, fg=False, term_colors=term_colors))
    if fields:
        return ANSI_ESC + '[' + ';'.join(fields) + 'm'
    return ''


def parse_ansi_sgr(ansi):  # pylint: disable=too-many-branches
    '''
    Attempt to parse an ANSI SGR escape sequence. This will return a dictionary
    with entries that parallel the keyword arguments of ansi_sgr with some
    modifications where necessary, or None if the sequence is not an
    ANSI SGR escape.
    '''
    kws = {
        'fg': None,
        'bg': None,
    }
    for k in ANSI_SGR_KEYWORDS_TO_PARAMS:
        kws[k] = False

    if not ansi.startswith(ANSI_ESC + '['):
        return None
    try:
        mi = ansi.find('m')
    except ValueError:
        return None
    else:
        values = ansi[2:mi].split(';')
        i = 0
        while True:
            try:
                d = values[i]
                try:
                    w = ANSI_SGR_PARAMS_TO_KEYWORDS[d]
                except KeyError:
                    tens, ones = divmod(int(d), 10)
                    if tens == 3:
                        k = 'fg'
                    elif tens == 4:
                        k = 'bg'
                    else:
                        continue

                    # 3x
                    # 4x
                    if ones < 8:
                        kws[k] = ones
                    elif ones == 8:
                        n = values[i + 1]
                        # 38;5
                        # 48;5
                        if n == '5':
                            kws[k] = int(values[i + 2])
                            i += 3
                            continue
                        # 38;2;r;g;b
                        # 48;2;r;g;b
                        if n == '2':
                            rgb = values[i + 2:i + 5]
                            kws[k] = tuple((int(x) / 0xff) for x in rgb)
                            i += 5
                            continue
                        # Unrecognized.
                        continue
                    # Unrecognized
                    else:
                        continue
                else:
                    kws[w] = True

            except IndexError:
                break
            else:
                i += 1
    return kws


def ansi_to_term_or_rgb(ansi, background=False):
    '''
    Attempt to parse a color value from an ANSI SGR escape sequence. This will
    return either a terminal color index or an RGB 3-tuple. Check the type.

    Parse background color if `background` is True.
    '''
    kws = parse_ansi_sgr(ansi)
    if not kws or kws['reset']:
        return None

    if background:
        return kws['bg']

    return kws['fg']


def term_to_ansi(color):
    '''
    Convert terminal color to an ANSI escape code.
    '''
    if color is None:
        return ansi_sgr(reset=True)
    return ansi_sgr(fg=color)


def ansi_to_term(ansi, term_colors=None):
    '''
    Attempt to convert an ANSI escape code to a terminal color index.
    '''
    color = ansi_to_term_or_rgb(ansi)
    if isinstance(color, int):
        return color

    if isinstance(color, tuple):
        return rgb_to_term(*color, colors=term_colors)

    return None


def rgb_to_ansi(r, g, b, background=False, term_colors=None):
    '''
    Convert RGB values to ANSI escapes.
    '''
    if background:
        return ansi_sgr(bg=(r, g, b), term_colors=term_colors)
    return ansi_sgr(fg=(r, g, b), term_colors=term_colors)


def ansi_to_rgb(ansi, background=False, term_colors=None):
    '''
    Attempt to convert an ANSI escape code to RGB values.
    '''
    color = ansi_to_term_or_rgb(ansi, background=background)
    if isinstance(color, tuple):
        return color

    if isinstance(color, int):
        return term_to_rgb(color, colors=term_colors)

    return None


# ------------------------------ Xonotic Colors ------------------------------ #

def xonotic_to_rgb(esc):
    '''
    Convert a Xonotic color escape code to RGB.

    These should omit the leading caret (^).
    '''
    try:
        esc = XONOTIC_SHORT_TO_LONG[esc]
    except KeyError:
        pass
    return tuple(int(x, 16) / float(0xf) for x in esc[1:4])


def rgb_to_xonotic(r, g, b, abbreviate=True):
    '''
    Convert RGB to a Xonotic color escape code.
    '''
    xon = 'x' + rgb_to_hex(r, g, b, width=1, include_hash=False)
    if abbreviate:
        try:
            xon = XONOTIC_LONG_TO_SHORT[xon]
        except KeyError:
            pass
    return '^' + xon


# ------------------------------- Wavelengths -------------------------------- #

def wavelength_to_rgb(w, bg=(0., 0., 0.), mode=None):  # pylint: disable=too-many-branches,too-many-statements,too-many-locals
    '''
    Convert a wavelength (in nanometers) to RGB.

    This is a work in progress and will never be truly accurate due to the
    limitations of how colors are displayed on screens. Relevant links:

      * http://mintaka.sdsu.edu/GF/explain/optics/rendering.html
      * http://www.fourmilab.ch/documents/specrend/
      * http://www.physics.sfasu.edu/astro/color/spectra.html
      * http://mintaka.sdsu.edu/GF/explain/optics/color/color.html

    Note that I have not read all of them yet, so I hae not (yet) taken all of the
    advice given.

    bg: The background color into which the values should fade.
    '''

    if 380.0 <= w < 440.0:
        r = -(w - 440.0) / (440.0 - 380.0)
        g = 0.0
        b = 1.0
    elif 440.0 <= w < 490.0:
        r = 0.0
        g = (w - 440.0) / (490.0 - 440.0)
        b = 1.0
    elif 490.0 <= w < 510.0:
        r = 0.0
        g = 1.0
        b = -(w - 510.0) / (510.0 - 490.0)
    elif 510.0 <= w < 580.0:
        r = (w - 510.0) / (580.0 - 510.0)
        g = 1.0
        b = 0.0
    elif 580.0 <= w < 645.0:
        r = 1.0
        g = -(w - 645.0) / (645.0 - 580.0)
        b = 0.0
    elif 645.0 <= w <= 780.0:
        r = 1.0
        g = 0.0
        b = 0.0
    else:
        r = 0.0
        g = 0.0
        b = 0.0

    if w > 700.0:
        f1 = max(0.0, 0.3 + 0.7 * (780.0 - w) / (780.0 - 700.0))
    elif w <= 420.0:
        f1 = max(0.0, 0.3 + 0.7 * (w - 380.0) / (420.0 - 380.0))
    else:
        f1 = 1.0

    r, g, b = (f1 * x for x in (r, g, b))

    if mode == 'lighten only':
        return tuple(max(x, y) for x, y in zip((r, g, b), bg))

    if mode == 'addition':
        return tuple(max(1.0, x + y) for x, y in zip((r, g, b), bg))

    if mode == 'scaled addition':
        x, y, z = (x + y for x, y in zip((r, g, b), bg))
        m = max(x, y, z)
        if m > 1.0:
            x /= m
            y /= m
            z /= m
        return x, y, z

    if mode == 'hls addition':
        h1, l1, s1 = rgb_to_hls(r, g, b)
        h2, l2, s2 = rgb_to_hls(*bg)
        # Additive lightness.
        max_l = max(l1, l2)  # min(1.0, l1+l2)
        # Project h, s to polar coordinates and addd them as vectors.
        rads = 2.0 * cmath.pi
        theta1 = h1 * rads
        theta2 = h2 * rads
        v1 = cmath.rect(s1, theta1)
        v2 = cmath.rect(s2, theta2)
        s, theta = cmath.polar(v1 + v2)
        h = theta / rads
        s = min(1.0, s)
        print((h1, l1, s1), (h2, l2, s2), (h, max_l, s))
        return hls_to_rgb(h, max_l, s)

    # overlay
    return r, g, b
#   f2 = 1.0 - f1
#   return tuple(x + f2*y for x,y in zip((r,g,b), bg))


# ----------------------------------- Main ----------------------------------- #

def run_example(words):
    '''
    Print some colored text to the terminal as an example.
    '''
    text = ' '.join(words)

    sys.stderr.write('Checking terminal color configuration...\n')
    term_colors = list(get_term_colors(timeout=100))
    if not term_colors:
        sys.exit("failed to get terminal colors")
    n_colors = len(term_colors)

    if not text:
        text = (
            f'The terminal appears to support {n_colors:d} colors. '
            'To display custom text, pass it as a command-line argument.'
        )

    text_len = len(text)
    try:
        for i in range(text_len):
            h = float(i) / text_len
            r, g, b = hsv_to_rgb(h, 1.0, 1.0)
            t = rgb_to_term(r, g, b, colors=term_colors)
            ansi = term_to_ansi(t)
            sys.stdout.write(ansi + text[i])
    finally:
        sys.stdout.write(term_to_ansi(None) + '\n')


if __name__ == '__main__':
    try:
        run_example(sys.argv[1:])
    except (KeyboardInterrupt, BrokenPipeError):
        pass
