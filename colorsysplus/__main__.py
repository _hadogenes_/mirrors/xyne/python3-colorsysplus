#!/usr/bin/env python3

# Copyright (C) 2012-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'''
Run the main function from colorsysplus as an example.
'''

import sys

from colorsysplus import run_example

if __name__ == '__main__':
    try:
        run_example(sys.argv[1:])
    except (KeyboardInterrupt, BrokenPipeError):
        pass
