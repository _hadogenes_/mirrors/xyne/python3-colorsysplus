#!/usr/bin/env python3

# Copyright (C) 2012-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'''
Formatters for colorized logging.
'''

import logging
import sys

from colorsysplus.format import (
    colorize_fmt as _colorize_fmt,
    format_with_color as _format_with_color
)


# -------------------------------- Constants. -------------------------------- #

# Default level colors.
LEVEL_COLORS = {
    logging.DEBUG: '00a0ff',
    logging.INFO: '00ff00',
    logging.WARNING: 'ffff00',
    logging.ERROR: 'ff0000',
    logging.CRITICAL: 'ff00ff'
}


# ------------------------- LevelDependentFormatter -------------------------- #

class LevelDependentFormatter(logging.Formatter):
    '''
    Format log messages differently depending on log level. This delegates to
    other formatters.
    '''
    def __init__(
        self,
        *args,
        formatters=None,
        **kwargs
    ):
        '''
        Args:
            *args:
                Positional Formatter arguments.

            formatters:
                A dictionary mapping log levels to formatter objects. The
                formatting of received messages will be delegated to these
                formatters if their level matches.

                Not all log levels are required. If a given message's level is
                absent from the dictionary, the format associated with the
                highest level below the message's level will be used. For
                example, if a format is given for logging.WARNING, it will be
                applied to logging.ERROR and logging.CRITICAL if these latter
                levels are not also given.

                If no level in the dictionary is below the message's level, the
                formatting will not be delegated.

            **kwargs:
                Keyword Formatter arguments.
        '''
        super().__init__(*args, **kwargs)
        if not formatters:
            formatters = dict()
        self.formatters = sorted(formatters.items())

    def format(self, record):
        level = record.levelno
        formatter = None
        for lvl, fmter in self.formatters:
            if lvl <= level:
                formatter = fmter
        if formatter is None:
            return super().format(record)
        return formatter.format(record)


# ------------------------------ ColorFormatter ------------------------------ #

class ColorFormatter(logging.Formatter):
    '''
    Format log messages with colors.
    '''
    def __init__(
        self,
        *args,
        fmt=None,
        style='%',
        output_type='ansi',
        color_char='@',
        **kwargs
    ):
        if fmt:
            fmt = _colorize_fmt(
                fmt,
                style=style,
                output_type=output_type,
                color_char=color_char
            )
        self.output_type = output_type
        super().__init__(*args, fmt=fmt, style=style, **kwargs)

    def format(self, record):
        # An adapter can optionally pass through a colored message format with
        # field values.
        try:
            fmt = record.color_fmt
            char = record.color_char
            style = record.color_style
            fields = record.color_fields
        except AttributeError:
            pass
        else:
            record.msg = _format_with_color(
                fmt,
                fields,
                style=style,
                output_type=self.output_type,
                color_char=char
            )
        return super().format(record)


# ---------------------------- configure_logging ----------------------------- #

def configure_logging(
    fmt=r'[{asctime:s@888}] {levelname:s@XXXXXX} {message:s}',
    style='{',
    datefmt='%Y-%m-%d %H:%M:%S',
    color_char='@',
    output_type='ansi',
    level_color_placeholder='XXXXXX',
    level_colors=None,
    **kwargs
):  # pylint: disable=too-many-arguments
    '''
    A wrapper around logging.basicConfig to configure colorized message logging.
    In addition to the argument accepted by that function, the following are
    also supported.

    fmt:
        The format string. This replaces the "format" argument for basicConfig.

    color_char:
        The custom format character for color codes in the chosen style.

    output_type:
        The output type. It must be one of the types supported by ColorizedText,
        or the custom type 'tty_ansi'. If the value is 'tty_ansi' then the
        output type will be set to 'ansi' for StreamHandlers without output
        streams that are TTYs, otherwise plain.

    level_color_placeholder:
        A placeholder string for the log-level-specific color. This will be
        replaced by the appropriate log level hexadecimal color string.

    level_colors:
        A map of logging levels to hexadecimal colors. These will be used to
        replace the value of "level_color_placeholder" in the format string for
        each level-specific formatter.
    '''
    if level_colors is None:
        level_colors = LEVEL_COLORS.copy()

    def get_formatter(otype):
        if level_colors:
            # This ensures that the level-dependent formatter always delegates. This
            # is necessary because it cannot handle custom color replacements in the
            # format string.
            formatters = dict(
                (
                    level,
                    ColorFormatter(
                        fmt=(
                            fmt.replace(level_color_placeholder, color)
                            if level_color_placeholder
                            else fmt.replace(f'{color_char}{level_color_placeholder}', '')
                        ),
                        style=style,
                        datefmt=datefmt,
                        color_char=color_char,
                        output_type=otype,
                    )
                )
                for (level, color) in level_colors.items()
            )
            return LevelDependentFormatter(formatters=formatters)
        return ColorFormatter(fmt=fmt, style=style, datefmt=datefmt)

    logging.basicConfig(**kwargs)
    logging._acquireLock()  # pylint: disable=protected-access
    try:
        if output_type == 'tty_ansi':
            ansi_formatter = get_formatter('ansi')
            plain_formatter = get_formatter('plain')
            for handler in logging.root.handlers:
                try:
                    if handler.stream.isatty():
                        handler.setFormatter(ansi_formatter)
                        continue
                except AttributeError:
                    pass
                handler.setFormatter(plain_formatter)
        else:
            formatter = get_formatter(output_type)
            for handler in logging.root.handlers:
                handler.setFormatter(formatter)
    finally:
        logging._releaseLock()  # pylint: disable=protected-access


# -------------------------------- # Example --------------------------------- #

def run_example():
    '''
    Print some colored logging messages.
    '''
    print('Examples\n')
    for output_type, desc in (
        ('plain', 'plain text without colors'),
        ('html', 'colored HTML'),
        ('ansi', 'text colored with ANSI escape codes'),
        ('tty_ansi', 'equivalent to "ansi" if the output is a TTY stream, otherwise "plain"')
    ):
        print('Output Type: {} ({})'.format(output_type, desc))
        configure_logging(level=logging.DEBUG, output_type=output_type, stream=sys.stdout)
        for level in ('debug', 'info', 'warning', 'error', 'critical'):
            func = getattr(logging, level)
            func('logging on level %s', level)
        print()


if __name__ == '__main__':
    try:
        run_example()
    except (KeyboardInterrupt, BrokenPipeError):
        pass
