#!/usr/bin/env python3

# Copyright (C) 2012-2021 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'''
Handle text colors in different output formats (ANSI, HTML, BBCode, etc.).
'''

import re
from colorsysplus import (
    ANSI_SGR_REGEX,
    get_term_colors,
    hsv_to_rgb,
    rgb_to_hex,
    hex_to_rgb,
    rgb_to_ansi,
    ansi_to_rgb,
    rgb_to_xonotic,
    xonotic_to_rgb,
    term_to_ansi,
)


# ------------------------------ Colorized Text ------------------------------ #

class ColorizedText():
    '''
    Colorized text stored as a list of (RGB, text) tuples, where RGB is a 3-tuple
    of RGB values in the range 0-1.

    Colorized text can be converted to multiple formats.
    '''
    TYPES = {
        'ansi': 'ANSI SGR sequences',
        'bbcode': 'BB forum code',
        'html': 'HTML',
        'plain': 'plain text without colors',
        'xonotic': 'Xonotic color escapes'
    }

    def __init__(self, chunks=None, query_term_rgb=False):
        '''
        chunks:
          An iterable of 2-tuples where the first item in each tuple is a 3-tuple
          of RGB values in the range of [0,1] and the second item is the string to
          color.

        query_term_rgb:
          If True, query the true RGB values of the currently set terminal colors.
        '''
        if chunks is None:
            chunks = list()
        else:
            chunks = list(chunks)
        self.chunks = chunks

        self.query_term_rgb = query_term_rgb
        self.term_colors = None

    def __repr__(self):
        return repr(self.chunks)

    def __str__(self):
        return ''.join(x[1] for x in self.chunks)

    def update(self, chunks):
        '''
        Update the object with new text.colorsysplus
        '''
        self.chunks = chunks

    def maybe_load_term_colors(self, force=False):
        '''
        Load terminal color RGB values.
        '''
        if self.query_term_rgb and (self.term_colors is None or force):
            self.term_colors = get_term_colors()

    def clear(self):
        '''
        Clear chunks.
        '''
        self.chunks.clear()

    def append(self, rgb, text, merge=False):
        '''
        Add an RGB code and a chunk of text.

        rgb may be None if the text is not colored, i.e. it should inherit the
        default color of its target output.
        '''
        try:
            # If the color is the same as the last chunk, append.
            if merge or self.chunks[-1][0] == rgb:
                self.chunks[-1] = (
                    self.chunks[-1][0],
                    self.chunks[-1][1] + text
                )
            # Else add a new chunk.
            else:
                self.chunks.append((rgb, text))
        except IndexError:
            self.chunks = [(rgb, text)]

    def reduce(self):
        '''
        Merge superfluous chunks.
        '''
        chunks = self.chunks
        self.chunks = list()
        for rgb, text in chunks:
            self.append(rgb, text)

    def to_x(self, typ):
        '''
        Convert to the given type. It should be listed in TYPES.
        '''
        if typ not in self.TYPES:
            raise ValueError('unsupported type: {}'.format(typ))
        return getattr(self, 'to_{}'.format(typ))()

    def from_x(self, typ, text):
        '''
        Parse the given type. It should be listed in TYPES.
        '''
        if typ not in self.TYPES:
            raise ValueError('unsupported type: {}'.format(typ))
        return getattr(self, 'from_{}'.format(typ))(text)

    def to_plain(self):
        '''
        Generate a plain text string without colors.
        '''
        return ''.join(span for (_, span) in self.chunks)

    def from_plain(self, text):
        '''
        Parse plain text without any colors. This will insert all of the text
        into a single chunk without color.
        '''
        self.chunks = [(None, text)]

    def to_bbcode(self):
        '''
        Generate a BBCode string.
        '''
        bbcode = ''
        for rgb, span in self.chunks:
            if rgb is None or span.isspace():
                bbcode += span
            else:
                hexstr = rgb_to_hex(*rgb)
                bbcode += r'[color={}]{}[/color]'.format(hexstr, span)
        return bbcode

    def from_bbcode(self, text):
        '''
        Parse a BBCode string.
        '''
        regex = re.compile(r'\[color=(.+?)\](.+?)\[/color\]', re.I)
        itr = iter(regex.split(text))
        self.clear()
        for uncolored in itr:
            if uncolored:
                self.append(None, uncolored)
            try:
                hexstr = next(itr)
                colored = next(itr)
                if colored:
                    self.append(hex_to_rgb(hexstr), colored)
            except StopIteration:
                pass

    def to_html(self):
        '''
        Generate an HTML string.
        '''
        text = ''
        for rgb, span in self.chunks:
            if rgb is None or span.isspace():
                text += span
            else:
                hexstr = rgb_to_hex(*rgb)
                text += r'<span style="color:{};">{}</span>'.format(hexstr, span)
        return text

    def from_html(self, text):
        '''
        Parse an HTML string.
        '''
        regex = re.compile(r'<span[^>]+style="(?:[^"]*;\s*)?color=([^;]+).*?>(.+?)</span>', re.I)
        itr = iter(regex.split(text))
        self.clear()
        for uncolored in itr:
            if uncolored:
                self.append(None, uncolored)
            try:
                hexstr = next(itr)
                colored = next(itr)
                if colored:
                    self.append(hex_to_rgb(hexstr), colored)
            except StopIteration:
                pass

    def to_ansi(self):
        '''
        Generate an ANSI-colored string.
        '''
        self.maybe_load_term_colors()
        text = ''
        colored = False
        reset = term_to_ansi(None)
        for rgb, span in self.chunks:
            if rgb is None:
                if colored:
                    text += reset + span
                else:
                    text += span
                colored = False
            else:
                ansi = rgb_to_ansi(*rgb, term_colors=self.term_colors)
                text += ansi + span
                colored = True
        if colored:
            text += reset
        return text

    def from_ansi(self, text):
        '''
        Parse an ANSI-colored string.
        '''
        self.maybe_load_term_colors()
        itr = iter(ANSI_SGR_REGEX.split(text))
        self.clear()
        try:
            uncolored = next(itr)
            if uncolored:
                self.append(None, uncolored)
        except StopIteration:
            return

        for ansi in itr:
            try:
                text = next(itr)
                if text:
                    rgb = ansi_to_rgb(ansi, term_colors=self.term_colors)
                    self.append(rgb, text)
            except StopIteration:
                return

    def to_xonotic(self, prefix=False, suffix=False, abbreviate=True):
        '''
        Generate a Xonotic string.

        prefix: If True, "^7" will be added to the beginning of strings that do not
        begin with a color code.

        suffix: If True, "^7" will be added to the end of the string if it contains
        any color codes.

        abbreviate: Use color code abbreviations where possible (e.g. "^1" for
        "^xf00")

        The defaults for prefix and suffix may change. Do not rely on them.
        '''
        text = ''
        try:
            needs_prefix = self.chunks[0][0] is None
        except IndexError:
            needs_prefix = False
        colored = False
        default = '^7'
        for rgb, span in self.chunks:
            if rgb is None:
                if colored:
                    text += default + span
                else:
                    text += span
            else:
                colored = True
                text += rgb_to_xonotic(*rgb, abbreviate=abbreviate) + span
        if colored:
            if prefix and needs_prefix:
                text = default + text
            if suffix:
                text += default
        return text

    def from_xonotic(self, text):
        '''
        Parse a Xonotic string.
        '''
        self.clear()
        # Avoid caret escapes.
        split_text = text.split('^^')
        text_len = len(split_text)
        for index in range(text_len):  # pylint: disable=too-many-nested-blocks
            if not split_text[index]:
                continue
            chunks = split_text[index].split('^')
            if chunks[0]:
                self.append(None, chunks[0])
            for chunk in chunks[1:]:
                try:
                    if chunk[0] in "0123456789":
                        if chunk[1:]:
                            rgb = xonotic_to_rgb(chunk[0])
                            self.append(rgb, chunk[1:])
                        continue
                    if chunk[0] == 'x':
                        if all(chunk[j].lower() in '0123456789abcdef' for j in range(1, 4)):
                            if chunk[4:]:
                                rgb = xonotic_to_rgb(chunk[0:4])
                                self.append(rgb, chunk[4:])
                            continue
                except IndexError:
                    pass
                self.append(None, '^' + chunk, merge=True)
            if text_len - index > 1:
                self.append(None, '^', merge=True)


# -------------------------------- Functions --------------------------------- #

def get_linear_gradient_func(grad):
    '''
    Return a function that accepts a numerical value in the range [0,1] and
    returns an RGB value corresponding to a point along a multicolored linear
    gradient.

    grad:
      A list or tuple of RGB tuples that define the gradient. The values are
      assumed to be evently spaced.
    '''

    def func(fraction):
        if fraction < 0:
            return grad[0]

        if fraction >= 1:
            return grad[-1]

        n_grad = len(grad)
        sub_f = fraction * (n_grad - 1)
        i = int(sub_f)
        sub_f -= i
        return tuple(x + sub_f * (y - x) for x, y in zip(grad[i], grad[i + 1]))

    return func


def apply_gradient(func, text, *args, **kwargs):
    '''
    Apply a gradient to a string and return chunks for ColorizedText.

    The function should accept a float in the range [0.0, 1.0] indicating the
    relative position in the string for which a color should be returned. args and
    kwargs will be passed as additional arguments to the function.
    '''
    text_len = len(text)
    chunks = list()
    for i in range(text_len):
        fraction = float(i) / (text_len - 1)
        rgb = func(fraction, *args, **kwargs)
        chunks.append((rgb, text[i]))
    return chunks


# ----------------------------------- Main ----------------------------------- #
def print_example():
    '''
    Print an example to stdout.
    '''
    ctext = ColorizedText()

    # Linear gradient example.
    grad1 = (
        (1, 0, 0.5),
        (1, 1, 0),
        (0, 1, 0),
        (0, 1, 1),
        (0, 0, 1)
    )
    grad1 += tuple(reversed(grad1[:-1]))
    text1 = 'This is an example of a custom linear gradient using a fixed number of colors.'

    grad2 = (
        (0.698, 0.132, 0.203),
        (1.0, 1.0, 1.0),
        (0.234, 0.233, 0.430)
    )
    text2 = 'This is another example of a linear gradient using fewer colors.'

    for grad, text in ((grad1, text1), (grad2, text2)):
        func = get_linear_gradient_func(grad)
        chunks = apply_gradient(func, text)
        ctext.update(chunks)
        print(ctext.to_ansi())

    # hsv_to_rgb example that shows how extra parameters can be used.
    chunks = apply_gradient(
        hsv_to_rgb,
        'This example uses the hsv_to_rgb function to apply the gradient.',
        0.75, 1.0
    )

    ctext.update(chunks)
    print(ctext.to_ansi())


if __name__ == '__main__':
    try:
        print_example()
    except (KeyboardInterrupt, BrokenPipeError):
        pass
