#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''colorsysplus''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1609574651)),
  description='''An extension of the standard colorsys module with support for CMYK, terminal colors, ANSI and more.''',
  author='''Xyne''',
  author_email='''ac xunilhcra enyx, backwards''',
  url='''http://xyne.archlinux.ca/projects/python3-colorsysplus''',
  packages=['''colorsysplus'''],
)
